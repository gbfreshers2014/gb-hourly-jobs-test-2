package routines;

import java.util.UUID;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;

public class UPSUtility {

	private final static DateTimeZone merchantTimezone = DateTimeZone
			.forID("-0500");
	public static final int INVALID_DATE = -1;

	private static final int STATE_UNIDENTIFIED = 0;
	private static final int STATE_GUEST = 1;
	public static final int STATE_FREE_TRIAL = 2;
	public static final int STATE_PAID = 3;
	private static final int STATE_PAID_PAST_DUE = 4;
	private static final int STATE_DELINQUENT = 5;
	public static final int STATE_CANCELLED = 6;

	public static final int CUSTOMER_TYPE_GENERAL = 0;
	public static final int CUSTOMER_TYPE_INTERNAL = 1;

	public static final String TAG_INTERNAL = "internal";

	/*
	 * INSERT INTO DIM_Customer_States values (1, "GUEST"); INSERT INTO
	 * DIM_Customer_States values (2, "FREE TRIAL"); INSERT INTO
	 * DIM_Customer_States values (3, "PAID"); INSERT INTO DIM_Customer_States
	 * values (4, "PAID PAST DUE"); INSERT INTO DIM_Customer_States values (5,
	 * "DELINQUENT"); INSERT INTO DIM_Customer_States values (6,
	 * "STATE_CANCELLED");
	 */

	public static String getNewUUID() {
		return UUID.randomUUID().toString();
	}

	public static boolean isCancelled(String tags) {
		return isTagFound(tags, "cancelled");
	}

	public static boolean isSubscriber(String tags) {
		return isTagFound(tags, "subscriber");
	}

	public static boolean isGuest(String tags, String state) {
		boolean isEnabled = "enabled".equals(state);
		return isEnabled && !isSubscriber(tags) && !isCancelled(tags);
	}

	public static boolean isGuestForStateDeduction(String tags, String state) {
		boolean isEnabled = "enabled".equals(state) || "invited".equals(state);
		return isEnabled && !isSubscriber(tags) && !isCancelled(tags);
	}

	public static boolean isCardLessUser(String tags) {
		// mainly bloggers
		return isTagFound(tags, "nocc");
	}

	public static boolean isDisabled(String customerState) {
		return "disabled".equalsIgnoreCase(customerState);
	}

	public static boolean isLike(String instr, String pattern) {
		if (pattern != null && instr != null) {
			return (instr.toLowerCase().indexOf(pattern) >= 0);
		}
		return false;
	}

	public static int getCustomerType(String customerName, String customerTags) {
		if (isLike(customerTags, TAG_INTERNAL)
				|| isLike(customerName, TAG_INTERNAL)) {
			return CUSTOMER_TYPE_INTERNAL;
		}
		return CUSTOMER_TYPE_GENERAL;
	}

	public static int getNewPreviousState(int oldCurrentState,
			int oldPreviousState, String customerTag, String customerState,
			String braintreeStatus, String paidThroughDateKey) {
		if (oldCurrentState == getCustomerState(oldCurrentState, customerTag,
				customerState, braintreeStatus, paidThroughDateKey)) {
			return oldPreviousState;
		} else {
			return oldCurrentState;
		}
	}

	public static boolean getResetSessionFlag(String tags, String state) {
		return isGuest(tags, state);
	}

	public static int getCustomerStateBraintree(int oldCurrentState,
			String customerTag, String customerState, String braintreeStatus,
			String paidThroughDateKey) {
		if (isCardLessUser(customerTag)) {
			return STATE_PAID;
		} else if (braintreeStatus == null) {
			if (isDisabled(customerState)) {
				return STATE_UNIDENTIFIED;
			}
			return STATE_GUEST;
			// // for customer who are not in braintree
			// if(oldCurrentState == 0) {
			// // newly introduced customers, hence older state is not available
			// return STATE_UNIDENTIFIED;
			// } else {
			// // customer previously present in DIM_Customer
			// return oldCurrentState;
			// }
		} else if ("current".equalsIgnoreCase(braintreeStatus)) {
			return STATE_PAID;
		} else if ("trial".equalsIgnoreCase(braintreeStatus)) {
			return STATE_FREE_TRIAL;
		} else if ("cancelled".equalsIgnoreCase(braintreeStatus)) {
			return STATE_CANCELLED;
		} else if ("notSubscribed".equalsIgnoreCase(braintreeStatus)) {
			return STATE_GUEST;
		} else if ("pastDue".equalsIgnoreCase(braintreeStatus)) {
			if (paidThroughDateKey == null || "".equals(paidThroughDateKey)) {
				return STATE_DELINQUENT;
			} else if (isValidDateKey(paidThroughDateKey)) {
				int diffDayCount = diffDaysFromNow(paidThroughDateKey);
				if (diffDayCount > 31) {
					return STATE_DELINQUENT;
				} else {
					return STATE_PAID_PAST_DUE;
				}
			}
		} else if (isDisabled(customerState)) {
			return STATE_UNIDENTIFIED;
		}
		return oldCurrentState;
	}

	public static boolean isGuest(int customer_state_key) {
		return customer_state_key == STATE_GUEST;
	}

	public static boolean isSubscriber(int customer_state_key) {
		return customer_state_key == STATE_FREE_TRIAL
				|| customer_state_key == STATE_PAID
				|| customer_state_key == STATE_PAID_PAST_DUE
				|| customer_state_key == STATE_DELINQUENT;
	}

	public static int getCustomerState(int oldCurrentState, String customerTag,
			String customerState, String braintreeStatus,
			String paidThroughDateKey) {
		if (isGuestForStateDeduction(customerTag, customerState)) {
			return STATE_GUEST;
		} else if (isDisabled(customerState)) {
			return STATE_UNIDENTIFIED;
		} else if (isCardLessUser(customerTag)) {
			return STATE_PAID; // bloggers;
		} else if (isCancelled(customerTag)) {
			return STATE_CANCELLED;
		} else if (braintreeStatus == null) {
			return oldCurrentState;
			// // for customer who are not in braintree
			// if(oldCurrentState == 0) {
			// // newly introduced customers, hence older state is not available
			// return STATE_UNIDENTIFIED;
			// } else {
			// // customer previously present in DIM_Customer
			// return oldCurrentState;
			// }
		} else if ("current".equalsIgnoreCase(braintreeStatus)) {
			return STATE_PAID;
		} else if ("trial".equalsIgnoreCase(braintreeStatus)) {
			return STATE_FREE_TRIAL;
		} else if ("pastDue".equalsIgnoreCase(braintreeStatus)
				&& isValidDateKey(paidThroughDateKey)
				&& diffDaysFromNow(paidThroughDateKey) <= 31) {
			return STATE_PAID_PAST_DUE;
		} else if ("pastDue".equalsIgnoreCase(braintreeStatus)
				&& ((paidThroughDateKey == null || ""
						.equals(paidThroughDateKey)) || (isValidDateKey(paidThroughDateKey) && diffDaysFromNow(paidThroughDateKey) > 31))) {
			return STATE_DELINQUENT;
		} else {
			return oldCurrentState; // return STATE_UNIDENTIFIED;
		}
	}

	public static boolean isValidDateKey(String dateKey) {
		try {
			if (dateKey != null) {
				DateTime.parse(dateKey, DateTimeFormat.forPattern("yyyyMMdd"));
				return true;
			}
		} catch (Exception e) {
		}
		return false;
	}

	public static int diffDaysFromNow(String dateKey) {
		try {
			if (dateKey != null) {
				DateTime d = DateTime.parse(dateKey,
						DateTimeFormat.forPattern("yyyyMMdd"));
				Days diff = Days.daysBetween(d, DateTime.now(merchantTimezone));
				return diff.getDays();
			}
		} catch (Exception e) {
		}
		return INVALID_DATE;
	}

	public static boolean isTagFound(String tags, String tagToSearch) {
		if (tags == null || tagToSearch == null) {
			return false;
		}
		String[] tagArray = tags.split(",");
		if ((tagArray != null) && (tagArray.length > 0)) {
			for (String tag : tagArray) {
				if (tag != null) {
					tag = tag.trim();
					if (tagToSearch.equals(tag)) {
						return true;
					}
				}
			}
		}
		return false;
	}
}
